export const getLogo = (code) => {
  return `https://i.ytimg.com/vi/${code}/hqdefault.jpg`
}

export const loadVideo = async ($axios, code) => {
  const url = `https://noembed.com/embed?url=http://www.youtube.com/watch?v=${code}`
  const resp = await $axios.get(url)
  if (resp.data.error) {
    throw new Error('Unable load video')
  }
  return resp.data
}
