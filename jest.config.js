module.exports = {
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/$1',
    '^~/(.*)$': '<rootDir>/$1',
    '^vue$': 'vue/dist/vue.common.js'
  },
  moduleFileExtensions: ['js', 'vue', 'json'],
  transform: {
    '^.+\\.js$': 'babel-jest',
    '.*\\.(vue)$': 'vue-jest',
  },

  collectCoverage: true,
  collectCoverageFrom: [
    '<rootDir>/pages/**/*.{vue,js}',
    '<rootDir>/layouts/**/*.{vue,js}',
    '<rootDir>/components/**/*.{vue,js}',
    '<rootDir>/plugins/**/*.{vue,js}',
    '<rootDir>/store/**/*.{vue,js}'
  ],

  coverageThreshold: {
    global: {
      lines: 80
    }
  }
}
