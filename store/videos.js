import Vue from 'vue'

const state = () => {
  return {
    list: []
  }
}

const mutations = {
  setList(state, data) {
    Vue.set(state, 'list', data)
  },
  add(state, data) {
    state.list.push(data)
  }
}

export default {
  state,
  mutations
}
