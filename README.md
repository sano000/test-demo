# test-samples

> Unit testing samples

## Build Setup

``` bash
# install jest
$ npm install -g jest

# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate

# run all tests
$ npm run test

# run solved tests
$ npm run solved

# run single test
$ jest test/task_01/task_01.test.spec.js
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).