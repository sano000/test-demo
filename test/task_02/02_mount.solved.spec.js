import { mount } from '@vue/test-utils'
import VideoList from '@/components/VideoList/VideoList.vue'

import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue)

describe('VideoList', () => {

  test('mounted', () => {
    const wrapper = mount(VideoList, {
      stubs: ['nuxt-link'],
      propsData: {
        items: getItems()
      }
    })

    expect(wrapper.findAll('img').length).toBe(3)
    expect(wrapper.findAll('img').at(0).attributes('src'))
      .toEqual('https://i.ytimg.com/vi/code1/hqdefault.jpg')
    expect(wrapper.findAll('img').at(1).attributes('src'))
      .toEqual('https://i.ytimg.com/vi/code2/hqdefault.jpg')
    expect(wrapper.findAll('img').at(2).attributes('src'))
      .toEqual('https://i.ytimg.com/vi/code3/hqdefault.jpg')
  })


  const getItems = () => {
    return [
      {
        title: 'Title 1',
        code: 'code1'
      },
      {
        title: 'Title 2',
        code: 'code2'
      },
      {
        title: 'Title 3',
        code: 'code3'
      }
    ]
  }
})
