import { shallowMount } from '@vue/test-utils'
import VideoList from '@/components/VideoList/VideoList.vue'

describe('VideoList', () => {

  test('shallowMount', () => {
    const wrapper = shallowMount(VideoList, {
      stubs: ['b-row', 'b-col'],
      propsData: {
        items: getItems()
      }
    })

    console.log(wrapper.html())
  })


  const getItems = () => {
    return [
      {
        title: 'Title 1',
        code: 'code1'
      },
      {
        title: 'Title 2',
        code: 'code2'
      },
      {
        title: 'Title 3',
        code: 'code3'
      }
    ]
  }
})
