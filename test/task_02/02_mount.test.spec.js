import { mount } from '@vue/test-utils'
import VideoList from '@/components/VideoList/VideoList.vue'

import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue)

describe('VideoList', () => {

  test('mounted', () => {
    const wrapper = mount(VideoList, {
      stubs: ['nuxt-link'],
      propsData: {
        items: getItems()
      }
    })

  })


  const getItems = () => {
    return [
      {
        title: 'Title 1',
        code: 'code1'
      },
      {
        title: 'Title 2',
        code: 'code2'
      },
      {
        title: 'Title 3',
        code: 'code3'
      }
    ]
  }
})
