import { shallowMount } from '@vue/test-utils'
import VideoList from '@/components/VideoList/VideoList.vue'

describe('VideoList', () => {

  test('shallowMount', () => {
    const wrapper = shallowMount(VideoList, {
      stubs: ['b-row', 'b-col'],
      propsData: {
        items: getItems()
      }
    })

    expect(wrapper.findAll('video-list-item-stub').length).toBe(3)
    expect(wrapper.findAll('video-list-item-stub').at(0).props('item')).toEqual({
      title: 'Title 1',
      code: 'code1'
    })
    expect(wrapper.findAll('video-list-item-stub').at(1).props('item')).toEqual({
      title: 'Title 2',
      code: 'code2'
    })
    expect(wrapper.findAll('video-list-item-stub').at(2).props('item')).toEqual({
      title: 'Title 3',
      code: 'code3'
    })
  })


  const getItems = () => {
    return [
      {
        title: 'Title 1',
        code: 'code1'
      },
      {
        title: 'Title 2',
        code: 'code2'
      },
      {
        title: 'Title 3',
        code: 'code3'
      }
    ]
  }
})
