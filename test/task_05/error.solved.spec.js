import { loadVideo } from '@/plugins/helper/video'

const methodWithError = (message) => {
  throw Error(message)
}

describe('Page video', () => {

  test('method with exception', () => {
    expect(() => methodWithError('Error message')).toThrow('Error message')
  })

  it('should throw exception if video not found', async () => {
    const get = jest.fn()
    get.mockResolvedValue({ data: { error: true }})

    await expect(loadVideo({ get }, 'code')).rejects.toThrow('Unable load video')
  })
})
