import { shallowMount } from '@vue/test-utils'
import Like from '@/components/Like/Like.vue'

describe('Like', () => {

  it('should reduce counter on dislike', () => {
    const wrapper = shallowMount(Like)

    wrapper.find('.like-down').trigger('click')
    wrapper.find('.like-down').trigger('click')
    expect(wrapper.find('.counter').text()).toBe('-2')
  })

  it('should increase counter on like', () => {
    const wrapper = shallowMount(Like)

    wrapper.find('.like-up').trigger('click')
    wrapper.find('.like-up').trigger('click')
    expect(wrapper.find('.counter').text()).toBe('2')
  })
})
