import { shallowMount } from '@vue/test-utils'
import Footer from '@/components/Layout/Footer.vue'

describe('Footer', () => {

  it('should be Vue component', () => {
    const wrapper = shallowMount(Footer)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should render date', () => {
    const wrapper = shallowMount(Footer)
    const expectValue = `© 2018 - ${ new Date().getFullYear() } Copyright`

    expect(wrapper.find('.footer-copyright').text()).toEqual(expectValue)
  })
})
