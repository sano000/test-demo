import { shallowMount } from '@vue/test-utils'
import flushPromises from 'flush-promises'
import page from '@/pages/product/_id.vue'

describe('Video page', () => {

  it('should open error page if video not found', async () => {
    const get = jest.fn()
    get.mockResolvedValue({ data: { error: true, message: 'Video not found' }})
    const error = jest.fn()
    const wrapper = shallowMount(page, {
      stubs: ['b-embed'],
      mocks: {
        $route: { params: { id: 1000 }},
        $axios: { get },
        $nuxt: { error }
      }
    })

    await flushPromises()

    expect(get).toHaveBeenCalledWith('https://noembed.com/embed?url=http://www.youtube.com/watch?v=1000')
    expect(error).toHaveBeenCalledWith('Unable load video')
  })
})
