import flushPromises from 'flush-promises'
import { shallowMount } from '@vue/test-utils'
import Default from '@/layouts/default.vue'

describe('Default Layout', () => {

  it('should load videos', () => {
    const wrapper = shallowMount(Default)

    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
