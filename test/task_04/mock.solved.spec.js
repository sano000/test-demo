import flushPromises from 'flush-promises'
import { shallowMount } from '@vue/test-utils'
import Default from '@/layouts/default.vue'

describe('Default Layout', () => {

  it('should load videos', async () => {
    const data = [
      {code: 'code'},
      {code: 'code2'},
    ]
    const get = jest.fn()
    get.mockResolvedValue({
      data
    })
    const commit = jest.fn()

    const wrapper = shallowMount(Default, {
      stubs: ['b-container', 'nuxt'],
      mocks: {
        $axios: { get },
        $store: { commit }
      }
    })
    await flushPromises()

    expect(wrapper.isVueInstance()).toBeTruthy()
    expect(commit).toHaveBeenCalledWith('videos/setList', data)
  })
})
