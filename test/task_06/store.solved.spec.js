import Vuex from 'vuex'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import videos from '@/store/videos'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('Store - videos', () => {

  const createStore = () => {
    return new Vuex.Store({
      modules: {
        videos: {
          ...videos,
          namespaced: true
        }
      }
    })
  }

  test('setList', async () => {
    const data = [
      {code: 'code1', title: 'title 1'},
      {code: 'code2', title: 'title 2'},
      {code: 'code3', title: 'title 3'}
    ]
    const store = createStore()

    store.commit('videos/setList', data)

    expect(store.state.videos.list).toEqual(data)
  })

  test('add', () => {
    const store = createStore()

    store.commit('videos/add', {code: 'code1', title: 'title 1'})
    expect(store.state.videos.list.length).toBe(1)

    store.commit('videos/add', {code: 'code2', title: 'title 2'})
    expect(store.state.videos.list.length).toBe(2)

    store.commit('videos/add', {code: 'code3', title: 'title 3'})
    expect(store.state.videos.list.length).toBe(3)

    expect(store.state.videos.list).toEqual([
      {code: 'code1', title: 'title 1'},
      {code: 'code2', title: 'title 2'},
      {code: 'code3', title: 'title 3'}
    ])
  })
})
