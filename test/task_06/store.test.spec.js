import Vuex from 'vuex'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import videos from '@/store/videos'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('Store - videos', () => {

  const createStore = () => {
    //...
  }

  test('setList', async () => {
    const data = [
      {code: 'code1', title: 'title 1'},
      {code: 'code2', title: 'title 2'},
      {code: 'code3', title: 'title 3'}
    ]
    const store = createStore()

  })

  test('add', () => {
    const store = createStore()

  })
})
