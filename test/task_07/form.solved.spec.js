import { mount } from '@vue/test-utils'
import add from '@/pages/product/add'

import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue)

describe('Add video page', () => {
  let getMock

  const createWrapper = (params) => {
    getMock = jest.fn()

    params || (params = {
      mocks: {
        $axios: {
          get: getMock
        }
      }
    })

    return mount(add, params)
  }

  it('should render inputs', () => {
    const wrapper = createWrapper()

    expect(wrapper.find('.input-title').exists()).toBeTruthy()
    expect(wrapper.find('.input-code').exists()).toBeTruthy()
  })

  it('should validate input', async () => {
    const wrapper = createWrapper()

    const errors = await wrapper.vm.validate({})

    expect(errors).toEqual({
      code: 'Code cannot be empty',
      title: 'Title cannot be empty'
    })
  })

  it('should validate video existance', async () => {
    const wrapper = createWrapper()
    getMock.mockResolvedValue({
      data: {error: true}
    })

    const errors = await wrapper.vm.validate({
      title: 'title',
      code: 'code'
    })

    expect(errors).toEqual({ code: 'Unable load video' })
  })

  it('should pass validation if values are correct', async () => {
    const wrapper = createWrapper()
    getMock.mockResolvedValue({
      data: {video: 'correct'}
    })

    const errors = await wrapper.vm.validate({
      title: 'title',
      code: 'code'
    })

    expect(errors).toEqual({})
  })

  it('should store form data', async () => {
    const validate = jest.fn()
    validate.mockResolvedValue({})
    const commit = jest.fn()
    const push = jest.fn()
    const wrapper = createWrapper({
      mocks: {
        $store: { commit },
        $router: { push }
      },
      methods: { validate },
      data: () => {
        return {
          form: {
            title: 'title',
            code: 'code'
          },
          errors: {}
        }
      }
    })

    await wrapper.vm.onSubmit()

    expect(validate).toHaveBeenCalledWith({code: 'code', title: 'title'})
    expect(commit).toHaveBeenCalledWith('videos/add', {code: 'code', title: 'title'})
    expect(push).toHaveBeenCalledWith({name: 'product-id', params: {id: 'code'}})
  })

  it('should submit form on click button', () => {
    const onSubmit = jest.fn()
    const wrapper = createWrapper({
      methods: { onSubmit }
    })

    wrapper.find('button').trigger('submit')

    expect(onSubmit).toHaveBeenCalled()
  })
})
